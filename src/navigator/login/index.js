import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import LoginScreen from '../../screens/auth/login';
import SignupScreen from '../../screens/auth/signup';
import OnboardingScreen from '../../screens/auth/onboarding';

const Stack = createStackNavigator();

const HomeStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Onboarding" 
        >
            <Stack.Screen
                name="Onboarding"
                component={OnboardingScreen}
                options={{
                    headerShown: false,
                    headerLeft: () => null,
                    headerTitle: 'Onboarding',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: true,
                    //headerLeft: () => null,
                    headerTitle: 'Login',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={SignupScreen}
                options={{
                    headerShown: true,
                    //headerLeft: () => null,
                    headerTitle: 'Sign Up',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    headerStyle: {
        //backgroundColor: colors.primary,
    }
})

export default HomeStack
import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import TripsScreen from '../../screens/trips';
import UpcomingTripDetails from '../../screens/trips/UpcomingTripDetails';
import CreateTrips from '../../screens/trips/CreateTrips';



const Stack = createStackNavigator();

const TripsStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Trips" 
        >
            <Stack.Screen
                name="Trips"
                component={TripsScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Trips',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
             <Stack.Screen
                name="CreateTrips"
                component={CreateTrips}
                options={{
                    headerShown: true,
                    headerLeftLabelVisible: false,
                    headerTitle: 'Create Trip',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="UpcomingTripDetails"
                component={UpcomingTripDetails}
                options={{
                    headerShown: true,
                    headerLeftLabelVisible: false,
                    headerTitle: 'Trip Details',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    headerStyle: {
        //backgroundColor: colors.primary,
    }
})

export default TripsStack
import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import Tabbar from './tabbar';
import LoginStack from './login';

import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();


const Navigator = ({}) => {

    const navigation = useNavigation()

    const user_logged_in = useSelector(state => state.auth.user_logged_in);


    useEffect(() => {
       
    }, [])


    return (
        <>
        <Stack.Navigator 
        initialRouteName={user_logged_in ? 'Tabbar' : 'LoginStack'}  
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
        
        </>
    );
}

export default Navigator
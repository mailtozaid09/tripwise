import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import HomeScreen from '../../screens/home';
import FavoritesScreen from '../../screens/favorites';

const Stack = createStackNavigator();

const HomeStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    headerLeft: () => null,
                    headerTitle: 'Home',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="Favorites"
                component={FavoritesScreen}
                options={{
                    headerShown: true,
                    //headerLeft: () => null,
                    headerTitle: 'Favorites',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
           
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    headerStyle: {
        //backgroundColor: colors.primary,
    }
})

export default HomeStack
import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, } from 'react-native'

import { colors } from '../../global/colors'

import HomeHeader from '../../components/header/HomeHeader'


import TripsListItem from '../../components/custom/home/TripsListItem'
import ActionButton from '../../components/button/ActionButton'
import { getNearbyPlaces } from '../../global/api'

import auth from '@react-native-firebase/auth';

const HomeScreen = ({navigation}) => {

    const [userDetails, setUserDetails] = useState({});

    const [topPlaces, setTopPlaces] = useState([]);
    const [attractions, setAttractions] = useState([]);
    const [parks, setParks] = useState([]);
    const [restaurants, setRestaurants] = useState([]);


    useEffect(() => {
        getUserDetails()
        getTopPlacesFunction()
        getAttractionsFunction()
        getParksFunction()
        getRestaurantsFunction()
    }, [])


    useEffect(() => {
        const backAction = () => {
            BackHandler.exitApp()
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
    }, [])

    
    const getUserDetails = () => {     
        auth().onAuthStateChanged((user) => {
            if (user) {
                setUserDetails(user)
            }
      });
    }


    const getTopPlacesFunction = () => {
        var term = 'Scenic Drives'
        getNearbyPlaces(term)
        .then((resp) => {
            setTopPlaces(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
                  location: x.location.city,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }

    const getAttractionsFunction = () => {
        var term = 'Tourist Attractions'
        getNearbyPlaces(term)
        .then((resp) => {
            setAttractions(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
                  location: x.location.city,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }

    const getParksFunction = () => {
        var term = 'National Parks'
        getNearbyPlaces(term)
        .then((resp) => {
            setParks(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
                  location: x.location.city,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }

    const getRestaurantsFunction = () => {
        var term = 'Restaurants'
        getNearbyPlaces(term)
        .then((resp) => {
            setRestaurants(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
                  location: x.location.city,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <HomeHeader userDetails={userDetails} />
            <ScrollView>
                <View style={styles.mainContainer} >
                    
                    <TripsListItem
                        title="For you"
                        data={topPlaces}
                    />

                    <TripsListItem
                        title="Restaurants"
                        data={restaurants}
                    />

                    <TripsListItem
                        title="National Parks"
                        data={parks}
                    />

                    <TripsListItem
                        title="Tourist Attractions"
                        data={attractions}
                    />

                    
                </View>
            </ScrollView>

            <ActionButton 
                type="add"
                onPress={() => {navigation.navigate('TripsStack', {screen: 'CreateTrips'})}} 
            />

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        paddingTop: 0,
    }
})

export default HomeScreen
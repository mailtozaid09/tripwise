import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'
import { colors } from '../../global/colors'
import { yelpAPIKey } from '../../global/constants'
import axios from 'axios'
import { getNearbyPlaces } from '../../global/api'


const FavoritesScreen = ({navigation}) => {

    const [places, setPlaces] = useState([]);


    useEffect(() => {
        getNearbyPlacesFunc()
    }, [])


    const getNearbyPlacesFunc = () => {
        var term = 'Mountain'
        getNearbyPlaces(term)
        .then((resp) => {
            setPlaces(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
            <View style={styles.mainContainer} >
                <Text>FavoritesScreen</Text>

                {places?.map((item) => (
                    <View key={item.id} >
                        <Text>{item.name}</Text>
                        <Image source={{uri: item?.image_url}}  style={{height: 200, }} />
                    </View>
                ))}
            </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
    }
})

export default FavoritesScreen
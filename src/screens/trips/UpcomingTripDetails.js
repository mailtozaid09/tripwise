import React, { useEffect, useRef, useState } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily';
import TripTodoPlan from '../../components/custom/trips/TripTodoPlans';
import TripOverview from '../../components/custom/trips/TripOverview';
import ViewToDoSheet from '../../components/bottomsheet/ViewToDoSheet';
import { getNumOfDays } from '../../global/constants';
import { useDispatch } from 'react-redux';

const UpcomingTripDetails = (props) => {
    const {navigation, route} = props;

    const refRBSheet = useRef();
    const dispatch = useDispatch()

    const trip_options = [
        'OVERVIEW', 'TO-DO PLAN'
    ]



    const [currectOption, setCurrectOption] = useState('OVERVIEW');
    
    const [tripDetails, setTripDetails] = useState({});

    const [daysArray, setDaysArray] = useState([]);
    const [todoListData, setTodoListData] = useState([]);
    const [todoDate, setTodoDate] = useState('');

    
    useEffect(() => {
        console.log("route.parmas=> ",route?.params?.params);
        
        var details = route?.params?.params
        setTripDetails(route?.params?.params)


        var numofdays = getNumOfDays(route?.params?.params?.tripStartDate, route?.params?.params?.tripEndDate)

        console.log("numofdays=> ",numofdays);

        var days_Array = Array(numofdays).fill().map((item, index) => (
            {
                day_number: index+1,
                tasks: []
            }
        ))
        setDaysArray(days_Array)
        console.log("days_Array=> ",days_Array);
    }, [])
    
    const getTodoListData = (data, date) => {
        console.log('data=> ',data);
       
        setTodoListData(data)
        setTodoDate(date)

        setTimeout(() => {
            refRBSheet.current.open();
        }, 1000);
    }

    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.tripOptionContainer} >
                {trip_options?.map((item, index) => (
                    <TouchableOpacity 
                        key={index}  
                        style={styles.tripOption} 
                        onPress={() => {setCurrectOption(item)}}
                    >
                        <Text style={[styles.tripOptionText, currectOption == item ? {color: colors.black, textDecorationLine: 'underline',} : {color: colors.gray}]} >{item}</Text>
                    </TouchableOpacity>
                ))}
            </View>
            <ScrollView>
                {currectOption == 'OVERVIEW'
                ?
                    <TripOverview tripDetails={tripDetails} />
                :
                    <TripTodoPlan tripDetails={tripDetails} daysArray={daysArray} openTodoList={(data, date) => getTodoListData(data, date)}  />
                }

            </ScrollView>

            <ViewToDoSheet
                refRBSheet={refRBSheet} 
                todoListData={todoListData}
                todoDate={todoDate}
                tripDetails={tripDetails}
                tripID={tripDetails?.id}
                // selectCurrentCategory={(category) => {
                //     selectCurrentCategory(category)
                // }} 
            /> 

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
    },
    tripName: {
        fontSize: 22, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    tripDestination: {
        fontSize: 18, 
        lineHeight: 20,
        fontFamily: Poppins.Regular,
        color: colors.black,
    },
    tripOptionContainer: {
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    tripOption: {
        marginRight: 15,
    },
    tripOptionText: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    }
})

export default UpcomingTripDetails
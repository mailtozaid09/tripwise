import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, Alert, } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { checkDate } from '../../global/constants'
import { trip_images } from '../../global/sampleData'

import Icon from '../../utils/icons'
import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'

import { addTripDetails } from '../../store/modules/home/actions'

import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment'
import DatePicker from 'react-native-date-picker'
import auth from '@react-native-firebase/auth';

import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const CreateTrips = ({navigation}) => {

    const dispatch = useDispatch();
    const [userDetails, setUserDetails] = useState({});

    const user_id = useSelector(state => state.auth.user_id);
   
    var datePlusOne = new Date()
    datePlusOne.setDate(datePlusOne.getDate() + 1);
    
    const [startDate, setStartDate] = useState(new Date());
    const [showStartDatePicker, setShowStartDatePicker] = useState(false);

    const [endDate, setEndDate] = useState(datePlusOne);
    const [showEndDatePicker, setShowEndDatePicker] = useState(false);
    

    const [tripImage, setTripImage] = useState(trip_images[0]);

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})

    useEffect(() => {
        getUserDetails()
    }, [])
    
    const getUserDetails = () => {     
        auth().onAuthStateChanged((user) => {
            if (user) {
                console.log("user=> > ",user);
                setUserDetails(user)
            }
        });
    }



    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    const saveTripFunction = () => {
  
        var isValid = true
                
        if(!form.tripName){
            console.log("Please enter a valid trip name");
            isValid = false
            setErrors((prev) => {
                return {...prev, tripName: 'Please enter a valid trip name'}
            })
        }

        if(!form.tripDestination){
            console.log("Please enter a valid trip destination");
            isValid = false
            setErrors((prev) => {
                return {...prev, tripDestination: 'Please enter a valid trip destination'}
            })
        }

        if(!checkDate(startDate, endDate)){
            console.log("Please enter a valid end date");
            isValid = false
            setErrors((prev) => {
                return {...prev, endDate: 'Please enter a valid end date'}
            })
        }

       
        if(isValid){
            dispatch(addTripDetails({
                tripName: form.tripName,
                tripDestination: form.tripDestination,
                tripImage: tripImage,
                tripStartDate: (startDate).toString(),
                tripEndDate: (endDate).toString(),
                userId: user_id,
            }))
            addTripDetailsToDB()
            navigation.navigate('Trips');
        }
    }

    const addTripDetailsToDB = () => {
        db.transaction(function (tx) {
            tx.executeSql(
                'INSERT INTO table_user (trip_name, trip_destination, trip_image, trip_start_date, trip_end_date, user_id) VALUES (?,?,?,?,?,?)',
                [form.tripName, form.tripDestination, tripImage, (startDate).toString(), (endDate).toString(), user_id],
                (tx, results) => {
                    console.log('Results =>>> ', results.rowsAffected);
                    if (results.rowsAffected > 0) {
                        console.log("Succes => ");
                    } else {
                        console.log("Error");
                        Alert.alert('Registration Failed');
                    }
                }
            );
        });
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <View style={styles.mainContainer} >
                    <Text style={styles.label} >Select Trip Image</Text>
                    <FlatList
                        data={trip_images}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={item => item}
                        renderItem={({item, index}) => (
                            <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => {setTripImage(item)}}
                                style={[styles.imageContainer, item == tripImage && {borderWidth: 2, borderColor: colors.primary}]}
                            >
                                <Image source={{uri: item}} style={styles.tripImage} />
                            </TouchableOpacity>
                        )}
                    />
                    
                    <Input
                        label="Trip Name"
                        placeholder="Enter the trip name"
                        error={errors.tripName}
                        onChangeText={(text) => {onChange({name: 'tripName', value: text,}); setErrors({}); }}
                    />

                    <Input
                        label="Trip Destination"
                        placeholder="Enter the trip destination"
                        error={errors.tripDestination}
                        onChangeText={(text) => {onChange({name: 'tripDestination', value: text,}); setErrors({}); }}
                    />

                    <Text style={styles.label} >Select Start Date</Text>
                    <View style={styles.descriptionContainer} >
                        <View style={styles.iconContainer} >
                            <Icon type={"Ionicons"} name={"calendar"}  size={32} color={colors.gray} />
                        </View>
                        <TouchableOpacity 
                            onPress={() => {setShowStartDatePicker(true)}}
                            style={{ flex: 1, padding: 10, borderRadius: 10, justifyContent: 'center'}} >
                            <Text style={{fontSize: 16, fontFamily: Poppins.Medium, color: colors.black,}} >{moment(startDate).format('Do MMM YYYY, h:mm a')}</Text>
                        </TouchableOpacity>
                      
                        <DatePicker
                            modal
                            date={startDate}
                            open={showStartDatePicker}
                            onConfirm={(date) => {
                                setShowStartDatePicker(false);
                                setStartDate(date);
                                setErrors({}); 
                            }}
                            onCancel={() => {
                                setShowStartDatePicker(false)
                            }}
                        />
                    </View>

                    <Text style={styles.label} >Select End Date</Text>
                    <View style={styles.descriptionContainer} >
                        <View style={styles.iconContainer} >
                            <Icon type={"Ionicons"} name={"calendar"}  size={32} color={colors.gray} />
                        </View>
                        <TouchableOpacity 
                            onPress={() => {setShowEndDatePicker(true)}}
                            style={{ flex: 1, padding: 10, borderRadius: 10, justifyContent: 'center'}} >
                            <Text style={{fontSize: 16, fontFamily: Poppins.Medium, color: colors.black,}} >{moment(endDate).format('Do MMM YYYY, h:mm a')}</Text>
                        </TouchableOpacity>

                       
                      
                        <DatePicker
                            modal
                            date={endDate}
                            open={showEndDatePicker}
                            onConfirm={(date) => {
                                setShowEndDatePicker(false);
                                setEndDate(date);
                                setErrors({}); 
                            }}
                            onCancel={() => {
                                setShowEndDatePicker(false)
                            }}
                        />
                    </View>
                    {errors?.endDate && <Text style={styles.error} >{errors?.endDate}</Text>}


                    <PrimaryButton
                        title="Save Trip"
                        onPress={() => {saveTripFunction()}}
                    />
                    
                </View>

                
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
    },
    label: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginBottom: 4,
    },
    title: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    error: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.reddish,
        marginTop: -6,
    },
    tripImage: {
        height: 120, 
        width: 200,
        borderRadius: 8,
    },
    imageContainer: {
        marginRight: 12, 
        marginBottom: 10, 
        borderRadius: 10,
    },
    descriptionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginBottom: 10,
        height: 60,
        paddingHorizontal: 15,
        paddingVertical: 4,
        borderWidth: 0.5,
        borderColor: colors.gray,
        borderRadius: 10,
    },
    iconContainer: {
        height: 40, 
        width: 40, 
        justifyContent: 'center', 
        alignItems: 'center',
        marginRight: 10
    },
})

export default CreateTrips
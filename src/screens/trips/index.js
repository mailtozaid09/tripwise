import React, {useEffect, useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'
import { colors } from '../../global/colors'
import TripsFilter from '../../components/custom/trips/TripsFilter';
import UpcomingTrips from '../../components/custom/trips/UpcomingTrips';
import { ongoing_trips, past_trips } from '../../global/sampleData';
import PastTrips from '../../components/custom/trips/PastTrips';
import { useSelector } from 'react-redux';


const TripsScreen = ({navigation}) => {

    const [currentTab, setCurrentTab] = useState('Upcoming');

    const trips_list = useSelector(state => state.home.trips_list);
    console.log("trips_list=> ",trips_list);

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
               
                <TripsFilter
                    currentTab={currentTab}
                    onChangeTab={(tab) => {setCurrentTab(tab)}}
                />

                {currentTab == 'Upcoming'
                ?
                <UpcomingTrips data={trips_list} />
                :
                <PastTrips data={past_trips} />
                }

                
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
    }
})

export default TripsScreen
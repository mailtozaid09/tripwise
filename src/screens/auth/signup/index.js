import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Alert, } from 'react-native'

import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily';

import Input from '../../../components/input';
import PrimaryButton from '../../../components/button/PrimaryButton';

import auth from '@react-native-firebase/auth';

import { useDispatch } from 'react-redux';
import { userId, userLoggedIn } from '../../../store/modules/auth/actions';
import AlertModal from '../../../components/modal/AlertModal';

import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const SignupScreen = ({navigation}) => {

    const dispatch = useDispatch()
    
    const [showEyeIcon, setShowEyeIcon] = useState(false);

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const [buttonLoader, setButtonLoader] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    const [showAlertModal, setShowAlertModal] = useState(false);

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }

    const signUpFunction = () => {
        setButtonLoader(true)
        
        var isValid = true
                
        if(!form.name){
            console.log("Please enter a valid name");
            isValid = false
            setButtonLoader(false)
            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid name'}
            })
        }
        
        if(!form.email){
            console.log("Please enter a valid email");
            isValid = false
            setButtonLoader(false)
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email'}
            })
        }

        if(!form.password){
            console.log("Please enter a valid password");
            isValid = false
            setButtonLoader(false)
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password'}
            })
        }

       
        if(isValid){
            registerUserToFirebase()
            registerUserToDB()
        }
    }
    
    const checkForErrors = (error) => {
            if (error.code === 'auth/email-already-in-use') {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("The email address is already in use!");
                setShowAlertModal(true)
                console.log('The email address is already in use!');
            
            }else if (error.code === 'auth/invalid-login') {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("The login credentials are invalid!");
                setShowAlertModal(true)
                console.log('The login credentials are invalid!');
            }else if (error.code === 'auth/invalid-email') {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("The email address is invalid!");
                setShowAlertModal(true)
                console.log('The email address is invalid!');
            }else if (error.code === "auth/user-not-found") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("User not found!");
                setShowAlertModal(true)
                console.log('User not found');
            }else if (error.code === "auth/weak-password") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("Password should be at least 6 characters!");
                setShowAlertModal(true)
                console.log('Password should be at least 6 characters!');
            }else {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("Something went wrong!");
                setShowAlertModal(true)
                console.log(error);
            }
    }


    const registerUserToFirebase = () => {
        auth()
        .createUserWithEmailAndPassword(form.email, form.password)
        .then((res) => {
            console.log('User account created & signed in! => ',res);
            
            dispatch(userId(res?.user?.uid))
            dispatch(userLoggedIn(true))
            updateUserProfile()
        })
        .catch(error => {
            console.error(error);
            setButtonLoader(false)
            checkForErrors(error)
        });
    }

    const registerUserToDB = () => {
        db.transaction(function (tx) {
            tx.executeSql(
                'INSERT INTO table_user (user_name, user_email, user_password) VALUES (?,?,?)',
                [form.name, form.email, form.password],
                (tx, results) => {
                    console.log('Results =>>> ', results.rowsAffected);
                    if (results.rowsAffected > 0) {
                        setButtonLoader(false)
                    } else {
                        setButtonLoader(false)
                        Alert.alert('Registration Failed');
                    }
                }
            );
        });
    }

    const updateUserProfile = async () => {
        const update = {
            displayName: form.name
        };
          
        await auth().currentUser.updateProfile(update);

        navigation.navigate('Tabbar');
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <View>
                    <Input
                        label="Name"
                        placeholder="Enter you name"
                        error={errors.name}
                        onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                    />
                    <Input
                        label="Email"
                        placeholder="Enter you email"
                        error={errors.email}
                        onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                    />

                    <Input
                        label="Password"
                        placeholder="Enter you password"
                        isPassword
                        error={errors.password}
                        onChangeEyeIcon={() => {setShowEyeIcon(!showEyeIcon)}}
                        onChangeText={(text) => {onChange({name: 'password', value: text,}); setErrors({}); }}
                        showEyeIcon={showEyeIcon}
                    />
                </View>
                
                <View>
                    <PrimaryButton
                        title="Sign Up" 
                        loader={buttonLoader}
                        onPress={() => {
                            signUpFunction()
                        }}
                    />
                    <View style={styles.alreadyContainer} >
                        <Text style={styles.alreadyText} >Already have an account? </Text>
                        <TouchableOpacity activeOpacity={0.5} onPress={() => {navigation.navigate('Login');}} >
                            <Text style={styles.highlighted} >Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            {showAlertModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    closeModal={() => {setShowAlertModal(false); }} 
                />
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    alreadyContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    highlighted: {
        textDecorationLine: 'underline',
        color: colors.primary,
    },
})

export default SignupScreen
import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'

import Input from '../../../components/input'
import PrimaryButton from '../../../components/button/PrimaryButton'
import { useDispatch } from 'react-redux'
import { userId, userLoggedIn } from '../../../store/modules/auth/actions'

import auth from '@react-native-firebase/auth';
import AlertModal from '../../../components/modal/AlertModal'

const LoginScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [showEyeIcon, setShowEyeIcon] = useState(false);

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const [buttonLoader, setButtonLoader] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    const [showAlertModal, setShowAlertModal] = useState(false);
    
    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    const loginFunction = () => {
        setButtonLoader(true)
        
        var isValid = true
                
        if(!form.email){
            console.log("Please enter a valid email");
            isValid = false
            setButtonLoader(false)
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email'}
            })
        }

        if(!form.password){
            console.log("Please enter a valid password");
            isValid = false
            setButtonLoader(false)
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password'}
            })
        }

       
        if(isValid){
            auth()
            .signInWithEmailAndPassword(form.email, form.password)
            .then((res) => {
                console.log("res =>>> ", res);
                if (res) {
                    dispatch(userId(res?.user?.uid))
                    dispatch(userLoggedIn(true))
                    navigation.navigate('Tabbar');
                }
                setButtonLoader(false)
            })
            .catch((error) => {
                console.log(error);
                setButtonLoader(false)
                if (error.code === 'auth/email-already-in-use') {
                    setAlertType('Error')
                    setAlertTile("Error!!!");
                    setAlertDescription("The email address is already in use!");
                    setShowAlertModal(true)
                    console.log('The email address is already in use!');
                }else if (error.code === 'auth/invalid-login') {
                    setAlertType('Error')
                    setAlertTile("Error!!!");
                    setAlertDescription("The login credentials are invalid!");
                    setShowAlertModal(true)
                    console.log('The login credentials are invalid!');
                }else if (error.code === 'auth/invalid-email') {
                    setAlertType('Error')
                    setAlertTile("Error!!!");
                    setAlertDescription("The email address is invalid!");
                    setShowAlertModal(true)
                    console.log('The email address is invalid!');
                }else if (error.code === "auth/user-not-found") {
                    setAlertType('Error')
                    setAlertTile("Error!!!");
                    setAlertDescription("User not found");
                    setShowAlertModal(true)
                    console.log('User not found');
                }else {
                    setAlertType('Error')
                    setAlertTile("Error!!!");
                    setAlertDescription(error);
                    setShowAlertModal(true)
                    console.log(error);
                }
            });
        }
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <View>
                    <Input
                        label="Email"
                        placeholder="Enter you email"
                        error={errors.email}
                        onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                    />

                    <Input
                        label="Password"
                        placeholder="Enter you password"
                        isPassword
                        error={errors.password}
                        onChangeEyeIcon={() => {setShowEyeIcon(!showEyeIcon)}}
                        onChangeText={(text) => {onChange({name: 'password', value: text,}); setErrors({}); }}
                        showEyeIcon={showEyeIcon}
                    />
                </View>
                
                <View>
                    <PrimaryButton
                        title="Login" 
                        loader={buttonLoader}
                        onPress={() => {
                            loginFunction()
                        }}
                    />
                    <View style={styles.alreadyContainer} >
                        <Text style={styles.alreadyText} >Don't have an account? </Text>
                        <TouchableOpacity activeOpacity={0.5} onPress={() => {navigation.navigate('SignUp');}} >
                            <Text style={styles.highlighted} >Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            {showAlertModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    closeModal={() => {setShowAlertModal(false); }} 
                />
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    alreadyContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    highlighted: {
        textDecorationLine: 'underline',
        color: colors.primary,
    },
})

export default LoginScreen
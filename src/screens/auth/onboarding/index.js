import React, {useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, } from 'react-native'

import { media } from '../../../global/media'
import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'
import { screenWidth } from '../../../global/constants'

import PrimaryButton from '../../../components/button/PrimaryButton'


const OnboardingScreen = ({navigation}) => {

    useEffect(() => {
        const backAction = () => {
            BackHandler.exitApp()
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
    }, [])
    
    

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imgContainer} >
                <Image source={media.getstarted} style={{height: screenWidth+100, width: screenWidth+100, resizeMode: 'contain', marginTop: 20}} />
                <Text style={styles.subtitle} >We believe that travelling around the world shouldn't be hard.</Text>
            </View>
            <PrimaryButton
                title="Get Started" 
                onPress={() => {
                    navigation.navigate('Login');
                }}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: colors.white,
    },
    imgContainer: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    subtitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textAlign: 'center'
    }
})

export default OnboardingScreen
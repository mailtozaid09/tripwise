import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Alert, Pressable, Modal, } from 'react-native'


import { Poppins } from '../../global/fontFamily';

import LottieView from 'lottie-react-native';
import { media } from '../../global/media';

import { colors } from '../../global/colors';
import Input from '../input';
import DatePicker from 'react-native-date-picker';
import Icon from '../../utils/icons';
import moment from 'moment';
import { addTaskDetails } from '../../store/modules/home/actions';
import { useDispatch } from 'react-redux';

const TaskModal = ({closeModal, tripID, tripDay, }) => {
    const dispatch = useDispatch();
  
    const [modalVisible, setModalVisible] = useState(true);

    const [time, setTime] = useState(new Date());
    const [showTimePicker, setShowTimePicker] = useState(false);


    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    const saveTripFunction = () => {
  
        var isValid = true
                
        if(!form.taskName){
            console.log("Please enter a valid task name");
            isValid = false
            setErrors((prev) => {
                return {...prev, taskName: 'Please enter a valid task name'}
            })
        }

        if(!form.taskDescription){
            console.log("Please enter a valid task destination");
            isValid = false
            setErrors((prev) => {
                return {...prev, taskDescription: 'Please enter a valid task destination'}
            })
        }
       
        if(isValid){
            dispatch(addTaskDetails({
                taskName: form.taskName, 
                taskDescription: form.taskDescription,
                taskDate: (time).toString(), 
                taskTime: (time).toString(),
                taskDay: tripDay,
                tripId: tripID,
                isTaskDone: false,
            }))
            // navigation.navigate('Trips');

            closeModal()
        }

    }
    

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Are you sure! \nYou wan to close the modal?');
                        closeModal()
                    }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <ScrollView nestedScrollEnabled={true} keyboardShouldPersistTaps >
                                <View style={{padding: 10, }} >
                                    <Text style={styles.modalLabel} >Create a task</Text>
                                    <Input
                                        label="Task Name"
                                        placeholder="Enter the task name"
                                        error={errors.taskName}
                                        onChangeText={(text) => {onChange({name: 'taskName', value: text,}); setErrors({}); }}
                                    />
                                     <Input
                                        label="Task Description"
                                        placeholder="Enter the task description"
                                        error={errors.taskDescription}
                                        onChangeText={(text) => {onChange({name: 'taskDescription', value: text,}); setErrors({}); }}
                                    />

                                    <Text style={styles.label} >Select Start Date</Text>
                                    <View style={styles.descriptionContainer} >
                                        <View style={styles.iconContainer} >
                                            <Icon type={"Ionicons"} name={"calendar"}  size={32} color={colors.gray} />
                                        </View>
                                        <TouchableOpacity 
                                            onPress={() => {setShowTimePicker(true)}}
                                            style={{ flex: 1, padding: 10, borderRadius: 10, justifyContent: 'center'}} >
                                            <Text style={{fontSize: 16, fontFamily: Poppins.Medium, color: colors.black,}} >{moment(time).format('h:mm a')}</Text>
                                        </TouchableOpacity>
                                    
                                        <DatePicker
                                            modal
                                            mode="time"
                                            date={time}
                                            open={showTimePicker}
                                            onConfirm={(date) => {
                                                setShowTimePicker(false);
                                                setTime(date);
                                                setErrors({}); 
                                            }}
                                            onCancel={() => {
                                                setShowTimePicker(false)
                                            }}
                                        />
                                    </View>
                                </View>
                                    <View style={{paddingHorizontal: 20, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: colors.white}]}
                                            onPress={closeModal}>
                                            <Text style={[styles.textStyle, {color: colors.primary}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                         <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonAdd]}
                                            onPress={() => saveTripFunction()}>
                                            <Text style={styles.textStyle}>Okay</Text>
                                        </TouchableOpacity>
                                    </View>
                            </ScrollView>

                        </View>
                    </View>
                </Modal>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute',
        backgroundColor: '#00000050',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
      },
    modalView: {
        height: 440,
        margin: 10,
        paddingVertical: 12,
        backgroundColor: colors.white,
        borderRadius: 20,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
      },
    button: {
        borderRadius: 30,
        height: 50,
        width: 100,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    buttonAdd: {
        backgroundColor: colors.primary,
    },
    buttonEdit: {
        backgroundColor: colors.primary,
    },
    buttonDelete: {

    },
    buttonClose: {
        borderWidth: 1,
        backgroundColor: colors.gray,
        borderColor: colors.primary,
    },
    textStyle: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.primary_light,
    },
    modalTitle: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    modalDescription: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    descriptionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginBottom: 10,
        height: 60,
        paddingHorizontal: 15,
        paddingVertical: 4,
        borderWidth: 0.5,
        borderColor: colors.gray,
        borderRadius: 10,
    },
    iconContainer: {
        height: 40, 
        width: 40, 
        justifyContent: 'center', 
        alignItems: 'center',
        marginRight: 10
    },
    modalLabel: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginBottom: 4,
        textAlign: 'center'
    },
    label: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginBottom: 4,
        textAlign: 'left'
    },
})

export default TaskModal
import React, {useEffect, useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'
import { getNearbyPlaces } from '../../../global/api'
import TripsListItem from '../home/TripsListItem'

const TripOverview = ({tripDetails}) => {

    const [topPlaces, setTopPlaces] = useState([]);
    const [activities, setActivities] = useState([]);


    useEffect(() => {
        getTopPlacesFunction()
        getActivitiesPlacesFunction()
    }, [])
    


    const getTopPlacesFunction = () => {
        var term = 'Scenic Drives'
        getNearbyPlaces(term)
        .then((resp) => {
            setTopPlaces(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
                  location: x.location.city,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }
    
    const getActivitiesPlacesFunction = () => {
        var term = 'Activities'
        getNearbyPlaces(term)
        .then((resp) => {
            setActivities(
                resp.businesses.map(x => ({
                  name: x.name,
                  image_url: x.image_url,
                  location: x.location.city,
              })),
            );
        })
        .catch((error) => {
            console.log(error);
        })
    }

    return (
        <View style={styles.container} >
            <Image source={{uri: tripDetails?.tripImage}} style={{height: 250}} />
            <View style={styles.mainContainer} >
                
                <Text style={styles.tripName} >{tripDetails?.tripName}</Text>
                <Text style={styles.tripDestination} >{tripDetails?.tripDestination}</Text>
                <Text style={styles.tripDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et pretium libero. Vivamus pretium felis eu est gravida, non lobortis ante volutpat. In sed fermentum urna. Vestibulum sed facilisis urna. Nunc sodales nisl varius odio consequat varius. Aliquam consequat massa vitae lorem varius, at venenatis purus sollicitudin. Pellentesque et lacus sit amet sapien consequat commodo ut sed ante. {'\n'} {'\n'} Aliquam accumsan mi ac purus gravida, vel dictum metus facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus ut faucibus nulla. Mauris fermentum risus eget rutrum maximus.</Text>


                <View>
                    <TripsListItem
                        title="Nearby Attractions"
                        data={topPlaces}
                    />

                    <TripsListItem
                        title="Nearby Activities"
                        data={activities}
                    />
                </View>
            
            </View> 
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    mainContainer: {
        padding: 20,
    },
    tripName: {
        fontSize: 22, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    tripDestination: {
        fontSize: 18, 
        lineHeight: 20,
        fontFamily: Poppins.Regular,
        color: colors.black,
    },
    tripDescription: {
        fontSize: 16, 
        marginTop: 20,
        fontFamily: Poppins.Regular,
        color: colors.black,
    }

})

export default TripOverview
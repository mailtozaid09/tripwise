import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { addDaysToDate, screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'
import moment from 'moment'

const TripTodoPlan = ({daysArray, tripDetails, openTodoList}) => {

    
    return (
        <View style={styles.container} >
            {daysArray?.map((item, index) => (
                <TouchableOpacity 
                    onPress={() => {openTodoList(item, addDaysToDate(tripDetails?.tripStartDate, index+1))}}
                    style={styles.dayContainer} >
                    <Text style={styles.dayTitle} >DAY {item?.day_number}</Text>
                    <Text style={styles.dateTitle} >{addDaysToDate(tripDetails?.tripStartDate, index+1)}</Text>
                </TouchableOpacity>
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 20,
        paddingTop: 0,
        flex: 1,
    },
    dayContainer: {
        width: '100%',
        height: 120,
        backgroundColor: colors.primary,
        //alignItems: 'center',
        padding: 20,
        justifyContent: 'center',
        borderRadius: 10,
        marginBottom: 15,
        flex: 1,
    },
    dayTitle: {
        fontSize: 28, 
        color: colors.white,
        fontFamily: Poppins.Medium,
    },
    dateTitle: {
        fontSize: 16, 
        color: colors.white,
        fontFamily: Poppins.Medium,
    }

})

export default TripTodoPlan
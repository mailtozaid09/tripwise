import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'

const TripsFilter = ({currentTab, onChangeTab, title}) => {

    
    return (
        <View style={styles.tabContainer} >
                <TouchableOpacity 
                    onPress={() => {onChangeTab('Upcoming')}}
                    activeOpacity={0.5}
                    style={[styles.tabStyle, currentTab == 'Upcoming' ? {backgroundColor: colors.primary,} : {}]}
                >
                    <Text style={[styles.title, currentTab == 'Upcoming' ? {color: colors.white,} : {color: colors.primary,}]} >Upcoming Trips</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => {onChangeTab('Past')}}
                    activeOpacity={0.5}
                    style={[styles.tabStyle, currentTab == 'Past' ? {backgroundColor: colors.primary,} : {}]}
                >
                    <Text style={[styles.title, currentTab == 'Past' ? {color: colors.white,} : {color: colors.primary,}]} >Past Trips</Text>
                </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    tabContainer: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 10,
        borderRadius: 10,
        marginBottom: 15,
        backgroundColor: colors.primary_light,
    },
    tabStyle: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        flex: 1,
    },
    title: {
        fontSize: 16, 
        fontFamily: Poppins.Medium,
    },


})

export default TripsFilter
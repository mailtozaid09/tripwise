import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { getNumOfDays, screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'
import { useNavigation } from '@react-navigation/native'
import moment from 'moment'
import { useSelector } from 'react-redux'

const UpcomingTrips = ({data, title}) => {

    const navigation = useNavigation()
    
    const user_id = useSelector(state => state.auth.user_id);

    return (
        <View style={styles.container} >
            <FlatList
                data={data}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item}
                renderItem={({item, index}) => ( 
                    <>
                        {user_id == item.userId &&
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={() => {navigation.navigate('UpcomingTripDetails', {params: item})}}
                            style={{padding: 15, marginBottom: 15, borderRadius: 10, backgroundColor: colors.primary_light}} key={index} >
                            <Image source={{uri:  item.tripImage}}  style={{height: 180, width: screenWidth-70, borderRadius: 10}} />
                            <View style={{marginTop: 10}} >
                                
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                    <Text style={styles.tripName}>{item.tripName}</Text>
                                    <Text style={styles.locationName}>{moment(item.tripStartDate).format('D')} - {moment(item.tripEndDate).format('D, MMM')}</Text>
                                </View>
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                    <Text style={styles.locationName}>{item.tripDestination}</Text>
                                    <Text style={styles.locationName}>{getNumOfDays(item.tripStartDate, item.tripEndDate)} days trip</Text>
                                    
                                </View>
                            </View>
                        </TouchableOpacity>
                        }
                    </>
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    titleContainer: {
        marginBottom: 5,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    title: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    viewAll: {
        fontSize: 14, 
        fontFamily: Poppins.Medium,
        color: colors.black,
        textDecorationLine: 'underline',
    },
    tripName: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    locationName: {
        fontSize: 16, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default UpcomingTrips
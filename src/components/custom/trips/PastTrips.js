import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'

const PastTrips = ({data, title}) => {

    
    return (
        <View style={styles.container} >
            <FlatList
                data={data}
                contentContainerStyle={{alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap'}}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item}
                renderItem={({item, index}) => ( 
                    <View style={{backgroundColor: colors.primary_light, borderRadius: 10, marginBottom: 15, width: screenWidth/2-30}} key={index} >
                        <Image source={{uri:  item.trip_image}}  style={{height: 140, width: '100%', borderRadius: 10}} />
                        <View style={{padding: 10}} >
                            <Text numberOfLines={1} style={styles.tripName}>{item.trip_name}</Text>
                            <Text numberOfLines={1} style={styles.locationName}>2 month ago</Text>
                        </View>
                    </View>
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: screenWidth-40,
        flex: 1,
    },
    titleContainer: {
        marginBottom: 5,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    title: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    viewAll: {
        fontSize: 14, 
        fontFamily: Poppins.Medium,
        color: colors.black,
        textDecorationLine: 'underline',
    },
    tripName: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    locationName: {
        fontSize: 16, 
        fontFamily: Poppins.Medium,
        color: colors.black,
        textDecorationLine: 'underline',
    },
})

export default PastTrips
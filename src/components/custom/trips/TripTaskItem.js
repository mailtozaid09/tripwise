import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'

import moment from 'moment'
import CheckBox from '@react-native-community/checkbox';
import Icon from '../../../utils/icons'
import { useDispatch } from 'react-redux'

const TripTaskItem = ({taskDetails, tripDetails, index, task_list, editTaskItem, deleteTaskItem}) => {

    const [toggleCheckBox, setToggleCheckBox] = useState(false)


   
    return (
        <View style={styles.container} index={index} >
            <View style={{flexDirection: 'row', }} >
                <View style={{alignItems: 'center', justifyContent: 'center'}} >
                    <CheckBox
                        disabled={false}
                        value={taskDetails?.isTaskDone}
                        onValueChange={(newValue) => {setToggleCheckBox(!taskDetails?.isTaskDone); editTaskItem(taskDetails, newValue)}}
                        style={{marginRight: 15,}}
                    />
                    <View style={{backgroundColor: task_list?.length - 1 > index ? colors.gray : null, flex: 1, marginTop: -2, marginRight: 15, width: 1}} >
                    </View>
                </View>

                <View style={{flex: 1,}} >
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                        <Text style={styles.taskTitle} >{moment(taskDetails?.taskTime).format('h:mm a')}</Text>
                        <TouchableOpacity onPress={() => {deleteTaskItem(taskDetails.id)}}>
                            <Icon type="MaterialCommunityIcons" name="delete" size={24} color={colors.gray} />
                        </TouchableOpacity>
                    </View>
                    <Text style={[styles.taskTitle, taskDetails?.isTaskDone && {textDecorationLine: 'line-through'}]} >{taskDetails?.taskName}</Text>
                    <Text style={[styles.taskSubTitle, {marginBottom: 15}, taskDetails?.isTaskDone && {textDecorationLine: 'line-through'}]} >{taskDetails?.taskDescription} </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // borderBottomWidth: 1,
        // marginBottom: 10,
        // alignItems: 'center',
        // justifyContent: 'space-between',
        // flexDirection: 'row',
        // padding: 10,
        // borderRadius: 10,
        // marginBottom: 15,
        // backgroundColor: colors.primary_light,
    },
    mainContainer: {
        padding: 20,
    },
    taskTitle: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    taskSubTitle: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.gray,
    },
})

export default TripTaskItem
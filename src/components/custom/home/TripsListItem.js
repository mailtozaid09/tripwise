import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import { colors } from '../../../global/colors'
import { screenWidth } from '../../../global/constants'
import { Poppins } from '../../../global/fontFamily'

const TripsListItem = ({data, title}) => {

    return (
        <View style={styles.container} >
            <View style={styles.titleContainer} >
                <Text style={styles.title} >{title}</Text>
                <TouchableOpacity 
                    onPress={() => {}}
                    activeOpacity={0.5}
                >
                    <Text style={styles.viewAll} >View all</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={data}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item?.id}
                renderItem={({item, index}) => ( 
                    <View style={{marginRight: 14, flex: 1, width: 270,}} key={item?.id} >
                        <Image source={{uri:  item?.image_url}}  style={{height: 180, width: 270, borderRadius: 10}} />
                        <View style={{marginTop: 10, flex: 1, }} >
                            <Text numberOfLines={1} style={styles.tripName}>{item?.name}</Text>
                            <Text style={styles.locationName}>{item?.location}</Text>
                        </View>
                    </View>
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //backgroundColor: colors.white,
    },
    titleContainer: {
        marginBottom: 5,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    title: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    viewAll: {
        fontSize: 14, 
        fontFamily: Poppins.Medium,
        color: colors.black,
        textDecorationLine: 'underline',
    },
    tripName: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    locationName: {
        fontSize: 16, 
        fontFamily: Poppins.Medium,
        color: colors.black,
        textDecorationLine: 'underline',
    },
})

export default TripsListItem
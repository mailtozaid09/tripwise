import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import Icon from '../../utils/icons';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';

import { Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';

const HomeHeader = ({userDetails}) => {

    const navigation = useNavigation()
    
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <TouchableOpacity 
                    onPress={() => {navigation.navigate('ProfileStack')}}
                    style={{flexDirection: 'row', alignItems: 'center'}} >
                    <View style={{height: 40, width: 40, borderWidth: 1, borderColor: colors.primary, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginRight: 12}} >
                        <Image source={media.user_active} style={{height: 20, width: 20, }} />
                    </View>
                    <View>
                        <Text style={styles.title} >Hello!</Text>
                        <Text style={styles.subtitle} >{userDetails?.displayName || 'HappyMonk'}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => {navigation.navigate('Notification')}}
                    style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Icon type={'FontAwesome'} name={'bell'}  size={26} color={colors.black} />
                    </TouchableOpacity>
            </View>
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        marginBottom: 10,
        paddingTop: 15,
        backgroundColor: colors.white,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subtitle: {
        fontSize: 18,
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default HomeHeader
import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, useColorScheme, } from 'react-native';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { Poppins } from '../../global/fontFamily';

import Icon from '../../utils/icons';

const Input = ({ placeholder, isSearch, label, error, value, onClearSearchText, onChangeText, onChangeEyeIcon, isPassword, showEyeIcon }) => {

    return (
        <View style={{marginBottom: 12,}} >
            <Text style={styles.label} >{label}</Text>

            <View style={[styles.inputContainer]}  >

                {isSearch && (
                    <View>
                        <Icon type="AntDesign" name="search1" size={28} color={colors.gray}  style={{marginRight: 10}} />
                    </View>
                )}

                <TextInput
                    placeholder={placeholder}
                    style={styles.inputStyle}
                    value={value}
                    onChangeText={onChangeText}
                    placeholderTextColor={colors.gray}
                    secureTextEntry={isPassword  && !showEyeIcon ? true : false}
                />

                {isPassword && (
                    <TouchableOpacity onPress={onChangeEyeIcon}>
                        {showEyeIcon 
                        ?
                            <Icon type="Feather" name="eye" size={24} color={colors.gray} />
                        :
                            <Icon type="Feather" name="eye-off" size={24} color={colors.gray} />
                        }
                    </TouchableOpacity>
                )}

                {isSearch && !value && (
                    <TouchableOpacity onPress={onClearSearchText}>
                        <Icon type="AntDesign" name="close" size={24} color={colors.gray} />
                    </TouchableOpacity>
                )}
            </View>
            {error && <Text style={styles.error} >{error}</Text>}
        </View>
    )
}


const styles = StyleSheet.create({
    inputContainer: {
        height: 60,
        paddingHorizontal: 15,
        paddingVertical: 4,
        borderWidth: 0.5,
        borderColor: colors.gray,
        borderRadius: 10,
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: 16,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: Poppins.Medium,
        color: colors.black,
        flex: 1,
    },
    iconImage: {
        height: 24,
        width: 24
    },
    label: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginBottom: 4,
    },
    error: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.reddish,
        marginTop: 4,
    },

})

export default Input

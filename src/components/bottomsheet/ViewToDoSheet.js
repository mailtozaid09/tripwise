import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import RBSheet from "react-native-raw-bottom-sheet";

import { fontSize, Poppins } from '../../global/fontFamily';

import { media } from '../../global/media';

import Icon from '../../utils/icons';
import { colors } from '../../global/colors';
import ActionButton from '../button/ActionButton';
import TaskModal from '../modal/TaskModal';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import TripTaskItem from '../custom/trips/TripTaskItem';
import { deleteTaskDetails, editTaskDetails } from '../../store/modules/home/actions';

const ViewToDoSheet = ({refRBSheet, tripDetails, todoDate, tripID, todoListData}) => {
    const dispatch = useDispatch()

    const task_list = useSelector(state => state.home.task_list);
    console.log("task_list=> ",task_list);


    const [showTaskModal, setShowTaskModal] = useState(false);

    const [taskListArray, setTaskListArray] = useState([]);

    useEffect(() => {
        // {tripDetails?.id}- {taskDetails?.tripId} -- {taskDetails?.taskDay}
        var newArray = task_list.filter((task,index) => {
            console.log("tripDetails => ",tripDetails);
            console.log("tripID= >>> ",tripID);
            console.log("task.tripId => ", task.tripId);
            console.log("tripDetails?.id => ", tripDetails?.id);
            console.log("task.taskDay => ", task.taskDay);
            console.log("todoListData?.day_number => ", todoListData?.day_number);

            return task.tripId == tripDetails?.id &&
                   task.taskDay == todoListData?.day_number 
        });

        

        console.log("newArray=> ",newArray);
        setTaskListArray(task_list)

    }, [task_list])

    

    const editTaskDetailsFunc = (task, val) => {
        console.log('====================================');
        console.log("=> task ",task);
        console.log('====================================');


        dispatch(editTaskDetails({
            id: task?.id,
            taskName: task?.taskName, 
            taskDescription: task?.taskDescription,
            taskDate: task?.taskDate, 
            taskTime: task?.taskTime,
            taskDay: task?.taskDay,
            tripId: task?.tripId,
            isTaskDone: val,
        }))
    }

    const deleteTaskDetailsFunc = (id) => {
        dispatch(deleteTaskDetails(id))
    }

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={500}
                onClose={() => {}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: colors.gray,
                        width: 100,
                    }, 
                    container: {
                        backgroundColor: 'white',
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        paddingTop: 10,
                    }
                }}
            >
                <View style={{flex: 1}} >
                    <ScrollView>
                        <View style={{alignItems: 'center', width: '100%', paddingHorizontal: 20, flex: 1, }} >
                            
                            <Text style={styles.dateNumber} >Day {todoListData?.day_number}</Text>
                            <Text style={styles.dateValue} >{todoDate}</Text>
                            <Text style={styles.locationTitle} >{tripDetails?.tripDestination}</Text>

                            <View style={{flex: 1, width: '100%', marginBottom: 20, }} >
                                {taskListArray && taskListArray?.map((item, index) => {
                                    if(item.tripId == tripDetails?.id && item.taskDay == todoListData?.day_number)
                                        return(
                                            <TripTaskItem 
                                                taskDetails={item} 
                                                index={index} 
                                                task_list={task_list} 
                                                tripDetails={tripDetails} 
                                                deleteTaskItem={(id) => deleteTaskDetailsFunc(id)}
                                                editTaskItem={(task, val) => editTaskDetailsFunc(task, val)}
                                            />
                                        )
                                    
                                })}
                                
                            </View>
                        </View>
                        

                    </ScrollView>
                    <View style={{flex: 1, width: '100%'}} >
                        <ActionButton
                            type="add"
                            onPress={() => {setShowTaskModal(true)}} 
                        />
                    </View>
                    
                    {showTaskModal && 
                        <TaskModal
                            tripDay={todoListData?.day_number}
                            tripID={tripDetails?.id}
                            onAdd={() => {setShowTaskModal(false); }}
                            closeModal={() => {setShowTaskModal(false); }} 
                        />
                    }
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    heading: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textAlign: 'center'
    },
    filterHeading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    dateNumber: {
        fontSize: 16,
        lineHeight: 18,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    dateValue: {
        fontSize: 24,
        marginTop: 6,
        lineHeight: 28,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    locationTitle: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.gray,
    },


})

export default ViewToDoSheet

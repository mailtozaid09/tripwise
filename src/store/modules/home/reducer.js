//import { dummyCategories, dummyExpenses } from "../../../global/sampleData";
import { ADD_TRIP_DETAILS, ADD_TASK_DETAILS, 
    DELETE_TRIP_DETAILS, DELETE_TASK_DETAILS, 
    EDIT_TRIP_DETAILS, EDIT_TASK_DETAILS 
} from "./actionTypes";

const INITIAL_STATE = {
    screen_loader: false,
    trips_list: [],
    task_list: [],
};

export default function home(state = INITIAL_STATE, action) {
    switch (action.type) {

    case ADD_TRIP_DETAILS: {
        const {
            id, tripName, tripDestination, tripImage, tripStartDate, tripEndDate, userId,
        } = action.payload
        return {
            ...state,
            trips_list: [
                ...state.trips_list, {
                    id, tripName, tripDestination, tripImage, tripStartDate, tripEndDate, userId,
                }
            ]
        }
    };


    case EDIT_TRIP_DETAILS:{
        const { tripName, tripDestination, tripImage, tripStartDate, tripEndDate, userId, } = action.payload
        const updatedTrip = state.trips_list.map(trip => {
        if (trip.id != action.payload.id) {
            return trip;
        } else {
            return {
            ...trip,
                tripName: tripName, 
                tripDestination: tripDestination,
                tripImage: tripImage, 
                tripStartDate: tripStartDate,
                tripEndDate: tripEndDate,
                userId: userId,
            };
        }
        });
    
    return {
        ...state,
        trips_list: updatedTrip
    };}

    case DELETE_TRIP_DETAILS: {
        const { id } = action.payload
            return {
                ...state,
                trips_list: state.trips_list.filter((trip) => trip.id != id)
        };
    }


    case ADD_TASK_DETAILS: {
        const {
            
            id, taskName, taskDescription, taskDate, taskDay, taskTime, tripId, isTaskDone,
        } = action.payload
        return {
            ...state,
            task_list: [
                ...state.task_list, {
                    id, taskName, taskDescription, taskDate, taskDay, taskTime, tripId, isTaskDone, 
                }
            ]
        }
    };


    case EDIT_TASK_DETAILS:{
        const { taskName, taskDescription, taskDate, taskDay, taskTime, tripId, isTaskDone, } = action.payload
        const updatedTask = state.task_list.map(task => {
        if (task.id != action.payload.id) {
            return task;
        } else {
            return {
            ...task,
                taskName: taskName, 
                taskDescription: taskDescription,
                taskDate: taskDate, 
                taskTime: taskTime,
                taskDay: taskDay,
                tripId: tripId,
                isTaskDone: isTaskDone,
            };
        }
        });
    
    return {
        ...state,
        task_list: updatedTask
    };}

    case DELETE_TASK_DETAILS: {
        const { id } = action.payload
            return {
                ...state,
                task_list: state.task_list.filter((task) => task.id != id)
        };
    }




    
    case '@home/SCREEN_LOADER':
        return {
            ...state,
            screen_loader: action.payload.screen_loader,
        };
        default:
        return state;
    }
}

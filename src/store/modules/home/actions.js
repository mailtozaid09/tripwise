import { 
	ADD_TRIP_DETAILS, EDIT_TRIP_DETAILS, DELETE_TRIP_DETAILS, 
	ADD_TASK_DETAILS, EDIT_TASK_DETAILS, DELETE_TASK_DETAILS,
} from './actionTypes';

let nextTripId = 0;
let nextTaskId = 0

export function addTripDetails(params) {
    return {
        type: ADD_TRIP_DETAILS,
        payload: {
            id: ++nextTripId,
			tripName: params.tripName, 
			tripDestination: params.tripDestination,
			tripImage: params.tripImage, 
			tripStartDate: params.tripStartDate,
			tripEndDate: params.tripEndDate,
			userId: params.userId,
        },
    };
};

export const editTripDetails = params => {
	return {
		type: EDIT_TRIP_DETAILS,
		payload: {
			id: params.id,
			tripName: params.tripName, 
			tripDestination: params.tripDestination,
			tripImage: params.tripImage, 
			tripStartDate: params.tripStartDate,
			tripEndDate: params.tripEndDate,
			userId: params.userId,
		},
	};
};

export const deleteTripDetails = id => {
  return {
		type: DELETE_TRIP_DETAILS,
		payload: {
			id
		},
	};
};



export function addTaskDetails(params) {
    return {
        type: ADD_TASK_DETAILS,
        payload: {
            id: ++nextTaskId,
			taskName: params.taskName, 
			taskDescription: params.taskDescription,
			taskDate: params.taskDate, 
			taskTime: params.taskTime,
			tripId: params.tripId,
			taskDay: params.taskDay,
			isTaskDone: params.isTaskDone,
        },
    };
};

export const editTaskDetails = params => {
	return {
		type: EDIT_TASK_DETAILS,
		payload: {
			id: params.id,
			taskName: params.taskName, 
			taskDescription: params.taskDescription,
			taskDate: params.taskDate, 
			taskTime: params.taskTime,
			tripId: params.tripId,
			taskDay: params.taskDay,
			isTaskDone: params.isTaskDone,
		},
	};
};

export const deleteTaskDetails = id => {
  return {
		type: DELETE_TASK_DETAILS,
		payload: {
			id
		},
	};
};



export function screenLoader(screen_loader) {
	return {
		type: '@home/SCREEN_LOADER',
		payload: {
			screen_loader,
		},
	};
}

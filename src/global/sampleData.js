import { colors } from "./colors";
import { media } from "./media";

export const trip_images = [
  'https://images.unsplash.com/photo-1618083707368-b3823daa2726?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjF8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1504280390367-361c6d9f38f4?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjN8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://plus.unsplash.com/premium_photo-1677002259522-111b3e74786f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjV8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1459231978203-b7d0c47a2cb7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MzV8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1450500392544-c2cb0fd6e3b8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NTN8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://plus.unsplash.com/premium_photo-1676982101887-49bb12d8a104?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NzJ8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1496080174650-637e3f22fa03?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTAxfHxhZHZlbnR1cmV8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
  'https://images.unsplash.com/photo-1616098851251-6cb03b884052?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OTF8fGFkdmVudHVyZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
]

export const ongoing_trips = [
    {
        "trip_id": "1",
        "trip_image": 'https://images.unsplash.com/photo-1496442226666-8d4d0e62e6e9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8bmV3JTIweW9ya3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Exploring NYC",
        "start_date": "2023-10-10",
        "end_date": "2023-10-15",
        "location": {
          "name": "New York City",
          "address": "New York, NY, USA"
        },
        "participants": [
          {
            "name": "Alice",
            "email": "alice@example.com"
          },
          {
            "name": "Bob",
            "email": "bob@example.com"
          }
        ],
        "budget": {
          "total": 3000,
          "currency": "USD"
        }
    },
]

export const past_trips = [
    {
        "trip_id": "1",
        "trip_image": 'https://images.unsplash.com/photo-1496442226666-8d4d0e62e6e9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8bmV3JTIweW9ya3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Exploring NYC",
        "start_date": "2023-10-10",
        "end_date": "2023-10-15",
        "location": {
          "name": "New York City",
          "address": "New York, NY, USA"
        },
        "participants": [
          {
            "name": "Alice",
            "email": "alice@example.com"
          },
          {
            "name": "Bob",
            "email": "bob@example.com"
          }
        ],
        "budget": {
          "total": 3000,
          "currency": "USD"
        }
    },
    {
        "trip_id": "2",
        "trip_image": 'https://images.unsplash.com/photo-1502602898657-3e91760cbb34?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8cGFyaXN8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Romantic Getaway in Paris",
        "start_date": "2023-09-20",
        "end_date": "2023-09-25",
        "location": {
          "name": "Paris",
          "address": "Paris, France"
        },
        "participants": [
          {
            "name": "David",
            "email": "david@example.com"
          },
          {
            "name": "Eva",
            "email": "eva@example.com"
          }
        ],
        "budget": {
          "total": 5000,
          "currency": "EUR"
        }
    },
    {
        "trip_id": "3",
        "trip_image": 'https://images.unsplash.com/photo-1552465011-b4e21bf6e79a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8VGhhaWxhbmR8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Thailand Adventure",
        "start_date": "2023-11-05",
        "end_date": "2023-11-20",
        "location": {
          "name": "Thailand",
          "address": "Bangkok, Thailand"
        },
        "participants": [
          {
            "name": "Grace",
            "email": "grace@example.com"
          },
          {
            "name": "Henry",
            "email": "henry@example.com"
          }
        ],
        "budget": {
          "total": 4000,
          "currency": "THB"
        }
    },
    {
        "trip_id": "4",
        "trip_image": 'https://images.unsplash.com/photo-1609184889514-656548112551?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8b3JsYW5kb3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Orlando Family Vacation",
        "start_date": "2023-12-15",
        "end_date": "2023-12-25",
        "location": {
          "name": "Orlando",
          "address": "Orlando, FL, USA"
        },
        "participants": [
          {
            "name": "Sarah",
            "email": "sarah@example.com"
          },
          {
            "name": "Michael",
            "email": "michael@example.com"
          },
          {
            "name": "Lucy",
            "email": "lucy@example.com"
          }
        ],
        "budget": {
          "total": 3500,
          "currency": "USD"
        }
    },
    {
        "trip_id": "5",
        "trip_image": 'https://plus.unsplash.com/premium_photo-1670596899123-c4c67735d77a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fEFscHN8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Skiing in the Alps",
        "start_date": "2023-02-10",
        "end_date": "2023-02-18",
        "location": {
          "name": "Swiss Alps",
          "address": "Zermatt, Switzerland"
        },
        "participants": [
          {
            "name": "James",
            "email": "james@example.com"
          },
          {
            "name": "Olivia",
            "email": "olivia@example.com"
          }
        ],
        "budget": {
          "total": 6000,
          "currency": "CHF"
        }
      },
      {
        "trip_id": "6",
        "trip_image": 'https://images.unsplash.com/photo-1496442226666-8d4d0e62e6e9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8bmV3JTIweW9ya3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Exploring NYC",
        "start_date": "2023-10-10",
        "end_date": "2023-10-15",
        "location": {
          "name": "New York City",
          "address": "New York, NY, USA"
        },
        "participants": [
          {
            "name": "Alice",
            "email": "alice@example.com"
          },
          {
            "name": "Bob",
            "email": "bob@example.com"
          }
        ],
        "budget": {
          "total": 3000,
          "currency": "USD"
        }
    },
    {
        "trip_id": "7",
        "trip_image": 'https://images.unsplash.com/photo-1502602898657-3e91760cbb34?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8cGFyaXN8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Romantic Getaway in Paris",
        "start_date": "2023-09-20",
        "end_date": "2023-09-25",
        "location": {
          "name": "Paris",
          "address": "Paris, France"
        },
        "participants": [
          {
            "name": "David",
            "email": "david@example.com"
          },
          {
            "name": "Eva",
            "email": "eva@example.com"
          }
        ],
        "budget": {
          "total": 5000,
          "currency": "EUR"
        }
    },
      
      
      
]

export const for_you_trips = [
    {
        "trip_id": "1",
        "trip_image": 'https://images.unsplash.com/photo-1496442226666-8d4d0e62e6e9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8bmV3JTIweW9ya3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Exploring NYC",
        "start_date": "2023-10-10",
        "end_date": "2023-10-15",
        "location": {
          "name": "New York City",
          "address": "New York, NY, USA"
        },
        "participants": [
          {
            "name": "Alice",
            "email": "alice@example.com"
          },
          {
            "name": "Bob",
            "email": "bob@example.com"
          }
        ],
        "budget": {
          "total": 3000,
          "currency": "USD"
        }
    },
    {
        "trip_id": "2",
        "trip_image": 'https://images.unsplash.com/photo-1502602898657-3e91760cbb34?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8cGFyaXN8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Romantic Getaway in Paris",
        "start_date": "2023-09-20",
        "end_date": "2023-09-25",
        "location": {
          "name": "Paris",
          "address": "Paris, France"
        },
        "participants": [
          {
            "name": "David",
            "email": "david@example.com"
          },
          {
            "name": "Eva",
            "email": "eva@example.com"
          }
        ],
        "budget": {
          "total": 5000,
          "currency": "EUR"
        }
    },
    {
        "trip_id": "3",
        "trip_image": 'https://images.unsplash.com/photo-1552465011-b4e21bf6e79a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8VGhhaWxhbmR8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Thailand Adventure",
        "start_date": "2023-11-05",
        "end_date": "2023-11-20",
        "location": {
          "name": "Thailand",
          "address": "Bangkok, Thailand"
        },
        "participants": [
          {
            "name": "Grace",
            "email": "grace@example.com"
          },
          {
            "name": "Henry",
            "email": "henry@example.com"
          }
        ],
        "budget": {
          "total": 4000,
          "currency": "THB"
        }
    },
    {
        "trip_id": "4",
        "trip_image": 'https://images.unsplash.com/photo-1609184889514-656548112551?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8b3JsYW5kb3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Orlando Family Vacation",
        "start_date": "2023-12-15",
        "end_date": "2023-12-25",
        "location": {
          "name": "Orlando",
          "address": "Orlando, FL, USA"
        },
        "participants": [
          {
            "name": "Sarah",
            "email": "sarah@example.com"
          },
          {
            "name": "Michael",
            "email": "michael@example.com"
          },
          {
            "name": "Lucy",
            "email": "lucy@example.com"
          }
        ],
        "budget": {
          "total": 3500,
          "currency": "USD"
        }
    },
    {
        "trip_id": "5",
        "trip_image": 'https://plus.unsplash.com/premium_photo-1670596899123-c4c67735d77a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fEFscHN8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60',
        "trip_name": "Skiing in the Alps",
        "start_date": "2023-02-10",
        "end_date": "2023-02-18",
        "location": {
          "name": "Swiss Alps",
          "address": "Zermatt, Switzerland"
        },
        "participants": [
          {
            "name": "James",
            "email": "james@example.com"
          },
          {
            "name": "Olivia",
            "email": "olivia@example.com"
          }
        ],
        "budget": {
          "total": 6000,
          "currency": "CHF"
        }
      }
         
      
      
      
]
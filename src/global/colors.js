export const colors = {
  primary: '#119FA3',
  primary_light: '#E3F3F9',
  primary_dark: '#65D5C5',

  black: '#0F0F0F',
  white: '#ffffff',


  cream: '#E8B88D',
  light_cream: '#F0E3D5',

  light_red: '#FEA7A9',

  dark_blue: '#212c46',
  
 
  
  gray: '#919399',
  light_gray: '#ebecf0',
  
  blue: '#81b3f3',
  red: '#f29999',
  orange: '#f7c191',
  reddish: '#ED4545',
}
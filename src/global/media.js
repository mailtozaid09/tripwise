export const media = {
    
    logo: require('../../assets/images/logo.png'),
    getstarted: require('../../assets/images/getstarted.png'),

    
    home: require('../../assets/icons/home.png'),
    home_active: require('../../assets/icons/home_active.png'),

    trips: require('../../assets/icons/trips.png'),
    trips_active: require('../../assets/icons/trips_active.png'),
    
    heart: require('../../assets/icons/heart.png'),
    heart_active: require('../../assets/icons/heart_active.png'),

    user: require('../../assets/icons/user.png'),
    user_active: require('../../assets/icons/user_active.png'),

    logout: require('../../assets/icons/logout.png'),


    dark_mode_lottie: require('../../assets/lottie/dark_mode_lottie.json'),
    empty_lottie: require('../../assets/lottie/empty_lottie.json'),
    edit_lottie: require('../../assets/lottie/edit_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    logout_lottie: require('../../assets/lottie/logout_lottie.json'),
    notification_lottie: require('../../assets/lottie/notification_lottie.json'),
    success_lottie: require('../../assets/lottie/success_lottie.json'),


}

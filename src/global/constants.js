import axios from "axios";
import moment from "moment";
import { Dimensions } from "react-native"


const radius = 4000;
const latitude = 37.773972
const longitude = -122.431297
const limit = 30;


export const yelpAPIKey =  "yNsNiHmzRE-MrT3pNtl_S56HPOocxl6kqS6onpMXu7Yajl1wDyBD94Mt3LvHvj1NQMztBtEdGVyT_mFwhXvKaADAmoIGdyt6WnVRK2ZmD16-d2zckn1slLTNVWm-XXYx"
export const client_id = "s2uAGm8GgL5tJ-gqvy1N9Q"


export const screenWidth = Dimensions.get('window').width
export const screenHeight = Dimensions.get('window').height


export const checkDate = (start, end) => {
    var mStart = moment(start);
    var mEnd = moment(end);
    return mStart.isBefore(mEnd);
}


export const getNumOfDays = (start, end) => {

    const date_1  =  moment(start).format('YYYY-MM-DD');
    const date_2    = moment(end).format('YYYY-MM-DD');
    
    const num_of_days = moment(date_2).diff(moment(date_1), 'days');
    
    return num_of_days
}


export const addDaysToDate = (start, days) => {
    var changeDate = moment(start).format('YYYY-MM-DD')
    var addDays = moment(changeDate).add(days, 'days');

    var formatDate = moment(addDays).format('D MMM, YYYY')
    
    return formatDate
}



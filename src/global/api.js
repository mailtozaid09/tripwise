const baseUrl = 'https://api.themoviedb.org/3'

const api_key = 'api_key=4d3b916ec1538ddd1e0aaaf160bda652'

const keyword = 'discover/movie'

const parameters = '&with_genres=35'

const tmdb_access_token = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0ZDNiOTE2ZWMxNTM4ZGRkMWUwYWFhZjE2MGJkYTY1MiIsInN1YiI6IjYyNDJiYWIxYzc0MGQ5MDA1ZDdhZDZkOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.-tMrDG6Bd6tfqtfBaHxRaaujDIBlz2vpHjdWP_HyamI'

const radius = 4000;
const latitude = 37.773972
const longitude = -122.431297
const limit = 30;
const yelpAPIKey =  "yNsNiHmzRE-MrT3pNtl_S56HPOocxl6kqS6onpMXu7Yajl1wDyBD94Mt3LvHvj1NQMztBtEdGVyT_mFwhXvKaADAmoIGdyt6WnVRK2ZmD16-d2zckn1slLTNVWm-XXYx"

const config = {
    headers: {
        Authorization: 'Bearer ' + yelpAPIKey,
    },
};


export function getNearbyPlaces(term) {
    console.log('====================================');
    console.log("getNearbyPlaces");
    console.log('====================================');
    return(
        fetch(
            `https://api.yelp.com/v3/businesses/search?term=${term}&latitude=` +
            latitude + '&longitude=' + longitude + '&radius=' + radius + '&limit=' + limit, config,
            {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${yelpAPIKey}`
                },
            }
        )
        .then(res => res.json())
    );
}


export function getTrendingMovies() {
    return(
        fetch(
            `${baseUrl}/trending/movie/day?${api_key}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Bearer ${tmdb_access_token}`
                },
            }
        )
        .then(res => res.json())
    );
}

export function getAllMovies() {
    return(
        fetch(
            `${baseUrl}/trending/all/day?${api_key}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Bearer ${tmdb_access_token}`
                },
            }
        )
        .then(res => res.json())
    );
}



export function getAllGenreList() {
    return(
        fetch(
            `${baseUrl}/genre/movie/list?${api_key}`,
            {
              method: "GET",
            }
        )
        .then(res => res.json())
    );
}

// export function getAllMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getPopularMovies() {
//     return(
//         fetch(
//             `${baseUrl}/movie/popular?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getTopRatedMovies() {
//     return(
//         fetch(
//             `${baseUrl}/movie/top_rated?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getUpcomingMovies() {
//     return(
//         fetch(
//             `${baseUrl}/movie/upcoming?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }


// export function getSimilarMovies(id) {
//     return(
//         fetch(
//             `${baseUrl}/movie/${id}/similar?${api_key}`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }



// export function getActionMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}&with_genres=28`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getComedyMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}&with_genres=35`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }

// export function getHorrorMovies() {
//     return(
//         fetch(
//             `${baseUrl}/${keyword}?${api_key}&with_genres=27`,
//             {
//               method: "GET",
//             }
//         )
//         .then(res => res.json())
//     );
// }
